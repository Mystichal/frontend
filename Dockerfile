FROM node:latest AS builder
WORKDIR /app
COPY . ./
RUN yarn
RUN yarn build

FROM nginx:alpine
RUN mkdir /etc/nginx/logs
COPY --from=builder /app/dist /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 80 443
CMD ["nginx", "-g", "daemon off;"]
